/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Reminder/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[26];
    char stringdata0[313];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 12), // "quitByAction"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 11), // "refreshView"
QT_MOC_LITERAL(4, 37, 5), // "event"
QT_MOC_LITERAL(5, 43, 14), // "readSocketData"
QT_MOC_LITERAL(6, 58, 18), // "onDisconnectSOcket"
QT_MOC_LITERAL(7, 77, 14), // "onMessageClick"
QT_MOC_LITERAL(8, 92, 10), // "closeEvent"
QT_MOC_LITERAL(9, 103, 12), // "QCloseEvent*"
QT_MOC_LITERAL(10, 116, 15), // "getIssuesSocket"
QT_MOC_LITERAL(11, 132, 18), // "onCombobox1Changed"
QT_MOC_LITERAL(12, 151, 5), // "index"
QT_MOC_LITERAL(13, 157, 18), // "onCombobox2Changed"
QT_MOC_LITERAL(14, 176, 13), // "connectToHost"
QT_MOC_LITERAL(15, 190, 9), // "onTimeOut"
QT_MOC_LITERAL(16, 200, 16), // "onTimeGetDataOut"
QT_MOC_LITERAL(17, 217, 21), // "StaticEnumWindowsProc"
QT_MOC_LITERAL(18, 239, 7), // "WINBOOL"
QT_MOC_LITERAL(19, 247, 4), // "HWND"
QT_MOC_LITERAL(20, 252, 4), // "hwnd"
QT_MOC_LITERAL(21, 257, 6), // "LPARAM"
QT_MOC_LITERAL(22, 264, 6), // "lParam"
QT_MOC_LITERAL(23, 271, 15), // "EnumWindowsProc"
QT_MOC_LITERAL(24, 287, 13), // "getAllAppName"
QT_MOC_LITERAL(25, 301, 11) // "getIdleTime"

    },
    "MainWindow\0quitByAction\0\0refreshView\0"
    "event\0readSocketData\0onDisconnectSOcket\0"
    "onMessageClick\0closeEvent\0QCloseEvent*\0"
    "getIssuesSocket\0onCombobox1Changed\0"
    "index\0onCombobox2Changed\0connectToHost\0"
    "onTimeOut\0onTimeGetDataOut\0"
    "StaticEnumWindowsProc\0WINBOOL\0HWND\0"
    "hwnd\0LPARAM\0lParam\0EnumWindowsProc\0"
    "getAllAppName\0getIdleTime"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   94,    2, 0x08 /* Private */,
       3,    1,   95,    2, 0x08 /* Private */,
       5,    0,   98,    2, 0x08 /* Private */,
       6,    0,   99,    2, 0x08 /* Private */,
       7,    0,  100,    2, 0x08 /* Private */,
       8,    1,  101,    2, 0x08 /* Private */,
      10,    0,  104,    2, 0x08 /* Private */,
      11,    1,  105,    2, 0x08 /* Private */,
      13,    1,  108,    2, 0x08 /* Private */,
      14,    0,  111,    2, 0x08 /* Private */,
      15,    0,  112,    2, 0x08 /* Private */,
      16,    0,  113,    2, 0x08 /* Private */,
      17,    2,  114,    2, 0x08 /* Private */,
      23,    1,  119,    2, 0x08 /* Private */,
      24,    0,  122,    2, 0x08 /* Private */,
      25,    0,  123,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void,
    0x80000000 | 18, 0x80000000 | 19, 0x80000000 | 21,   20,   22,
    0x80000000 | 18, 0x80000000 | 19,   20,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->quitByAction(); break;
        case 1: _t->refreshView((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->readSocketData(); break;
        case 3: _t->onDisconnectSOcket(); break;
        case 4: _t->onMessageClick(); break;
        case 5: _t->closeEvent((*reinterpret_cast< QCloseEvent*(*)>(_a[1]))); break;
        case 6: _t->getIssuesSocket(); break;
        case 7: _t->onCombobox1Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->onCombobox2Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: { bool _r = _t->connectToHost();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 10: _t->onTimeOut(); break;
        case 11: _t->onTimeGetDataOut(); break;
        case 12: { WINBOOL _r = _t->StaticEnumWindowsProc((*reinterpret_cast< HWND(*)>(_a[1])),(*reinterpret_cast< LPARAM(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< WINBOOL*>(_a[0]) = std::move(_r); }  break;
        case 13: { WINBOOL _r = _t->EnumWindowsProc((*reinterpret_cast< HWND(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< WINBOOL*>(_a[0]) = std::move(_r); }  break;
        case 14: _t->getAllAppName(); break;
        case 15: _t->getIdleTime(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
